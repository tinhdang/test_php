<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Meta Responsive -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <!-- My CSS -->
  <link rel="stylesheet" type="text/css" href="{!!url('public/public/css/style.css')!!}">
  <!-- Reset CSS -->
  <link rel="stylesheet" type="text/css" href="{!!url('public/public/css/reset.css')!!}">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400i,600,600i,700i" rel="stylesheet">
  <title>Test-GG</title>
</head>
<body>
  <header>
      <nav class="navbar container navbar-inverse">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="" class="navbar-brand logo">BLACK & WHITE</a>
        </div>
        <div class="navbar-collapse collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
              <li><a id="active" href="">Home</a></li>
              <li><a href="">About</a></li>
              <li><a href="">Shop</a></li>
              <li><a href="">Vacancies</a></li>
              <li><span class="glyphicon glyphicon-align-justify icon"></span></li>
            </ul>
        </div>
      </nav>
  </header>
  <div class="container-fluid">
    <div class="row">
       <img class="img-banner" src="{!!url('public/public/images/image1.png')!!}" alt="Image-Banner">
    </div>
  </div>
  <article>
    <section class="container article-top">

    <!-- Introduction-->
      @yield('intro')
    <!-- End Introduction-->
    </section>
    <section class="article-bottom">
      <div class="container">
        <div class="text-featured col-md-6 col-md-offset-4 col-sm-12">
          <p>Read some of our  <b>featured articles</b> </p>
        </div>
        <div class="row">
          <div class="col-md-8 col-sm-8 img-com">
          <img class="img-articles" src="{!!url('public/public/images/image2.png')!!}" alt="Image-articles">
          <p class="likes">172 <span class="glyphicon glyphicon-heart"></span></p>
          <p class="date">Januari'15 <b>South France</b></p>
        </div>
        <div class="col-md-4 col-sm-4 img-com">
          <img class="img-articles" src="{!!url('public/public/images/image3.png')!!}" alt="Image-articles">
          <p class="likes">112 <span class="glyphicon glyphicon-heart"></span></p>
          <p class="date">Januari'15 <b>Budapest</b></p>
        </div>
        </div>
        <div class="row subcribe">
          <div class="col-md-5">
            <p class="text-letters">Subscribe to our <b>newletters</b></p>
          </div>
          <div class="col-md-7">
            <span class="text-sendmail col-md-8 col-sm-8 col-xs-8">Send your email address</span>
            <input type="submit" value="Subscribe" class="input-subscribe col-md-3 col-sm-4 col-xs-4">
          </div>
        </div>
      </div>
    </section>
  </article>
  <footer>
    <div class="container ctn-footer">
      <div class="row">
      <div class="block-footer col-md-3 col-sm-6">
        <h4 class="title-footer">Black & White</h4>
        <p>consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. </p>
      </div>
      <div class="block-footer col-md-3 col-sm-6">
        <h4 class="title-footer">Contact</h4>
        <p>www.loremipsumdolar.nl </p>
        <p>182 AB Your Street Name </p>
        <p>e-mail@loremipsu</p>
        <p>+(021) 012 345 6789</p>
      </div>
      <div class="block-footer col-md-3 col-sm-6">
        <h4 class="title-footer">Latest Tweet</h4>
        <p class="last-tweet">“...aerat vluptatem. Ut eni et illi dolore magnam aliqua et aer</p>
        <p class="hours">3 hours ago</p>
      </div>
      <div class="block-footer col-md-3 col-sm-6">
        <h4 class="title-footer">Newletters</h4>
        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod....</p>
        <p>
          <span class="email-address col-md-8 col-sm-8 col-xs-8">E-mail address</span>
          <input type="submit" value="Subscribe" class="footer-subscribe col-md-4 col-sm-4 col-xs-4">
        </p>
      </div>
      </div>
    </div>
    <div class="container">
      <div class="bottom row">
        <span class="copy col-xs-12 col-md-4 ">&copy; 2015 Ruben Stom</span>
        <span class="right col-xs-3 col-md-1 col-md-offset-4">Vacancies</span>
        <span class="right  col-xs-3 col-md-1">Shop</span>
        <span class="right  col-xs-3 col-md-1">About</span>
        <span class="right  col-xs-3 col-md-1">Home</span>
      </div>
    </div>
  </footer>
</body>
</html>
