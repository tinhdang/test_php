@extends('public.master')
@section('intro')
 @foreach($data as $item)
<div class="row">
  <div class="col-md-5">
      <p class="text-title">{!! $item->title!!}</p>
  </div>
  <div class="col-md-6 col-md-offset-1">
      <p class="text-content"> {!! $item->content!!}</p>
  </div>
</div>
@endforeach()
@endsection()
