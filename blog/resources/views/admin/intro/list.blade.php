@extends('admin.master')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Introduction
      <small>List</small>
    </h1>
  </div>
  <!-- /.col-lg-12 -->
  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
      <tr align="center">
        <th>#</th>
        <th>Title</th>
        <th>Content</th>
        <th>Delete</th>
        <th>Edit</th>
      </tr>
    </thead>
    <tbody>
      <?php $stt=0; ?>
      @foreach($data as $item)
      <?php $stt=$stt+1; ?>
      <tr class="even gradeC" align="center">
        <td>{!! $stt!!}</td>
        <td>{!! $item->title !!}</td>
        <td>{!! $item->content !!}</td>
        <td class="center"><i class="fa fa-trash-o  fa-fw"></i>
          <form action="{!! URL::action('IntroductionController@destroy',['id'=> $item->id])!!}" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="_method" value=DELETE>
            <input type="hidden" name="_token" value="{!! csrf_token()!!}">
            <a href="{!!action('IntroductionController@destroy',$item->id)!!}">
            <input type="submit" name="" value="Delete" /> </a>
          </form>
        </td>
        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{!! URL::action('IntroductionController@edit',['id'=> $item->id])!!}">Edit</a></td>
      </tr>
      @endforeach()
    </tbody>
  </table>
</div>
@endsection()
