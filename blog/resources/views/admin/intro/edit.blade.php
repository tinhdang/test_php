@extends('admin.master')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Introductions
      <small>Edit</small>
    </h1>
  </div>
  <!-- /.col-lg-12 -->
  <div class="col-lg-7" style="padding-bottom:120px">
    @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <form action="{!! URL::action('IntroductionController@update',['id'=>$intro_id])!!}" enctype="multipart/form-data" method="POST">
    <input type="hidden" name="_method" value="PATCH">
    <input type="hidden" name="_token" value="{!! csrf_token()!!}">
      <div class="form-group">
        <label>Title</label>
        <input class="form-control" name="txtTitle" placeholder="Please Enter Title" value="{!! old('txtTitle',isset($data)?$data['title']:null)!!}" />
      </div>
      <div class="form-group">
        <label>Content</label>
        <textarea class="form-control" rows="3" name="txaContent">{!! $data['content']!!}</textarea>
      </div>
      <button type="submit" class="btn btn-default">Edit</button>
      <button type="reset" class="btn btn-default">Reset</button>
    <form>
  </div>
</div>
@endsection()
