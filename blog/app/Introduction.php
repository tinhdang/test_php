<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Introduction extends Model
{
    protected $table = 'introducs';
    protected $fillable = ['title','content'];
    public $timestamps = true;
}
