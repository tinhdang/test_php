<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IntroductionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtTitle'=>'required',
            'txaContent'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'txtTitle.required'=>'A title is required',
            'txaContent.required'=>'A content is required',
        ];
    }
}
