<?php

namespace App\Http\Controllers;

use App\Http\Requests\IntroductionRequest;
use Illuminate\Http\Request;
use App\Introduction;
class IntroductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Introduction::all();
        return view('admin.intro.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.intro.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IntroductionRequest $request)
    {
        $intro= new Introduction;
        $intro->title=$request->txtTitle;
        $intro->content=$request->txaContent;
        $intro->save();
        return redirect()->action('IntroductionController@index')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Add Introduction']);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Introduction::find($id)->toArray();
        $intro_id=Introduction::find($id);
        return view('admin.intro.edit',compact('data','intro_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IntroductionRequest $request, $id)
    {
        $intro=Introduction::find($id);
        $intro->title=$request->txtTitle;
        $intro->content=$request->txaContent;
        $intro->save();
        return redirect()->action('IntroductionController@index')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Edit Introduction']);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $intro = Introduction::find($id);
        $intro->delete();
        return redirect()->action('IntroductionController@index')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Edit Introduction']);;
    }
}
