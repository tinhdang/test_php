<?php

namespace App\Http\Controllers;
use App\Introduction;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index() {
        $data=Introduction::orderBy('id','DESC')->take(5)->get();
        return view('public.pages.intro',compact('data'));
    }
}
